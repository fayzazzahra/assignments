
package assignments.assignment1;

import java.lang.Math;
import java.util.ArrayList;
import java.util.Scanner;

/**
* This HammingCode Program accepts a binary input, then:
* 1. encodes it using the Hamming Code system;
* 2. decodes it to the original form.
* @author  Fayza Azzahra Robby
* @version 1.0
* @since 2020-02-15
*/
public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;
    
    /** 
     * This method is used to determine the amount of redundant bits
   * for the encode function.
     * @param data This is the array of strings of the original input.
     * @return r This returns an integer of the amount of redundant bits.
     */
    public static Integer calcRedundant(ArrayList<String> data) {
        boolean state = true;
        int r = 1;
        while (state == true) {
            if (Math.pow(2,r) >= data.size() + r + 1) {
                state = false; 
            } else {
                r += 1; 
            }
        }
        return r;
    }
    
    /** 
     * This method is used to determine the amount of redundant bits
   * for the decode function.
     * @param data This is the array of strings of the original input.
     * @return r: This returns an integer of the amount of redundant bits.
     */
    public static Integer calcRedundantDecode(ArrayList<String> data) {
        boolean state = true;
        int r = 1;
        while (state == true) {
            if (Math.pow(2,r) >= data.size()) {
                state = false;
            } else {
                r += 1;
            }
        }
        return r;
    }

    /**
    * This method is used to determine the positions
   * of the redundant bits, and replacing it with 0.
    * @param data This is the array of strings of the original input.
    * @param r This is the number of redundant bits.
    * @return res: This returns an array with the redundant bit positions replaced by 0.
   */
    public static ArrayList<String> posRedundanBit(ArrayList<String> data, int r) {
        int j = 0; //yang jadi power
        int k = 0; //indeks dari data yang aslinya
        int m = data.size();
        ArrayList<String> res = new ArrayList<String>();
        for (int i = 1; i <= m + r; i++) {
            if (i == Math.pow(2,j)) {
                res.add(String.valueOf(0));
                j += 1;
            } else {
                if (k >= m) {
                    break; 
                } else {
                    res.add(data.get(k));
                    k += 1;
                }
            }  
        }
        return res;
    }

    /** 
     * This method is used to convert the input data
   * from string to an array.
     * @param data This is the input that needs to be converted
     * @return res This returns an array of numbers from the input.
     */
    public static ArrayList<String> convertToList(String data) {
        ArrayList<String> res = new ArrayList<String>();
        for (char ch : data.toCharArray()) { 
            res.add(String.valueOf(ch)); 
        } 
        return res;
    }

    /** 
     * This method is used to convert the final data
   * from array to string, for the output.
     * @param data This is the input that needs to be converted
     * @return res This returns a string of numbers for the output.
     */
    public static String convertToString(ArrayList<String> data) {
        String res = new String("");
        for (String x: data) {
            res += x;
        }
        return res;
    }

    /** 
     * This method is used to check if the sum of 
   * the skipped bits are even or not.
     * @param data This is the input that need to be checked
     * @return true or false boolean.
     */
    public static boolean isEven(ArrayList<String> data) {
        int counter = 0;
        for (int i = 0; i < data.size();i++) {
            if (data.get(i).equals(String.valueOf(1))) {
                counter++;
            }
        }
        if (counter % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    /** 
     * This method is used to remove the parity bits.
     * @param data This is the array that needs to have the parity bits removed.
     * @param par This is the amount of redundant bits that needs to be removed.
     * @return newList This returns the final decoded code.
     */
    public static ArrayList<String> removeParity(ArrayList<String> data, int par) {
        ArrayList<String> listPar = new ArrayList<String>();
        for (int i = 0; i < par; i++) {
            listPar.add(String.valueOf((int)Math.pow(2,i) - 1));
        }
        ArrayList<String> newList = new ArrayList<String>();
        for (int i = 0; i < data.size();i++) {
            if (listPar.contains(String.valueOf(i))) {
                continue;
            } else {
                newList.add(data.get(i));
            }
        }
        return newList;
    }
    
    /** 
     * This method is used to encode a binary number using Hamming Code.
     * @param data This is the input that needs to be encoded.
     * @return res This is the final encoded binary number.
     */
    public static String encode(String data) {
        ArrayList<String> arr = convertToList(data);
        int r = calcRedundant(arr);
        ArrayList<String> listBit = posRedundanBit(arr,r);
        for (int i = 0; i < r;i++) {
            int bitRedundan = (int)Math.pow(2,i);
            int countSkipped = 0;
            ArrayList<String> temp = new ArrayList<String>();    
            boolean up = true;
            for (int j = bitRedundan - 1; j < listBit.size(); j++) {
                if (up == true) {
                    temp.add(listBit.get(j));
                    if (countSkipped == bitRedundan - 1) {
                        up = false;
                    }
                    countSkipped++; 
                } else {       
                    countSkipped--;
                    if (countSkipped == 0) {
                        up = true;
                    }
                }    
            }
            boolean isEven = isEven(temp);
            if (isEven == true) {
                listBit.set(bitRedundan - 1,String.valueOf('0'));
            } else {
                listBit.set(bitRedundan - 1, String.valueOf('1'));
            }
        }
        String res = convertToString(listBit);
        return res;
    }

    /** 
     * This method is used to decode a binary number back to
   * its original number using Hamming Code.
     * @param code This is the input code that needs to be decoded.
     * @return res This is the final decoded. binary number.
     */
    public static String decode(String code) {
        ArrayList<String> listBit = convertToList(code);
        int r = calcRedundantDecode(listBit);
        int bitError = 0;
        for (int i = 0; i < r; i++) {
            int bitRedundan = (int)Math.pow(2,i);
            int countSkipped = 0;
            ArrayList<String> temp = new ArrayList<String>();    
            boolean up = true;
            for (int j = bitRedundan - 1; j < listBit.size();j++) {
                if (up == true) {
                    temp.add(listBit.get(j));
                    if (countSkipped == bitRedundan - 1) {
                        up = false;
                    }
                    countSkipped++;
                } else {       
                    countSkipped--;
                    if (countSkipped == 0) {
                        up = true;
                    }
                }    
            }
            boolean isEven = isEven(temp);
            if (isEven == false) {
                bitError += bitRedundan; 
            }
        }
        if (bitError != 0) {
            if (listBit.get(bitError - 1).equals(String.valueOf('0'))) {
                listBit.set(bitError - 1, String.valueOf('1'));
            } else if (listBit.get(bitError - 1).equals(String.valueOf('1'))) {
                listBit.set(bitError - 1, String.valueOf('0'));
            }
        }
        listBit = removeParity(listBit,r);
        String res = convertToString(listBit);
        return res;
    }

    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}