package assignments.assignment2;

/**
* Seorang mahasiswa peserta kuliah diidentifikasi dengan ​NPM dan ​nama​nya.
* Ia memiliki serangkaian ​KomponenPenilaian yang berisi nilai-nilai pekerjaannya.
* @author  Fayza Azzahra Robby
* @version 1.0
* @since 2020-03-12
*/
public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    /**
     * Membuat constructor untuk Mahasiswa.
     * @param npm npm objek mahasiswa.
     * @param nama nama mahasiswa.
     * @param komponenPenilaian dari class KomponenPenilaian.
     */
    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    /**
     * Membuat constructor untuk KomponenPenilaian
     * @param namaKomponen nama komponen penilaian yang akan diambil.
     * @return nama komponen penilaian.
     */
    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        for (KomponenPenilaian nilai : komponenPenilaian) {
            if (nilai.getNama().equals(namaKomponen)) {
                return nilai;
            }
        }
        return null;
    }
    
    /**
     * Mengembalikan npm siswa yang dicari.
     * @return npm siswa.
     */
    public String getNpm() {
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    /**
     * Merekap semua rerata komponen penilaian, huruf indikator serta kelulusan.
     * @return rekapan komponen penilaian tiap mahasiswa.
     */
    public String rekap() {
        String y = "";
        double x = 0.0;
        for (KomponenPenilaian nilai : this.komponenPenilaian) {
            x += nilai.getNilai();
            y += String.format("Rerata %s: %.2f \n", nilai.getNama(), nilai.getRerata());
        }
        y += String.format ("Nilai akhir : %.2f \n", x);
        y += String.format("Huruf : %s", getHuruf(x)) + "\n";
        y += getKelulusan(x) + "\n";
        return y;
    }

    /**
     * Override toString method untuk print data mahasiswa.
     * @return npm dan nama mahasiswa.
     */
    public String toString() {
        return String.format("%s - %s", this.npm, this.nama);
        }

    /**
     * Mendapatkan detail tiap komponen penilaian, nilai akhir, serta kelulusan.
     * @return detail tiap komponen, nilai akhir, kelulusan.
     */
    public String getDetail() {
        String komponen = "";
        for (int i = 0; i < komponenPenilaian.length; i++) {
            if (komponenPenilaian[i] != null) {
                komponen += komponenPenilaian[i].getDetail() + "\n";
            }
        }
        double x = 0.0;
        String y = "";
        for (KomponenPenilaian nilai : this.komponenPenilaian) {
            x += nilai.getNilai();
        }
        y+= komponen + "Nilai Akhir: " + String.format("%.2f", x) + "\n" + "Huruf: " + getHuruf(x) + "\n" + getKelulusan(x);
        return y;
    }

    /**
     * Override method compareTo untuk membandingkan npm dua mahasiswa.
     * @return int hasil pembandingan npm.
     */
    @Override
    public int compareTo(Mahasiswa other) {
        return this.getNpm().compareTo(other.getNpm());
    }
}
