package assignments.assignment2;

/**
* ButirPenilaian memiliki ​nilai dan indikator apakah pekerjaan 
* terkait butir tersebut dikumpulkan terlambat atau tidak.
* @author  Fayza Azzahra Robby
* @version 1.0
* @since 2020-03-12
*/
public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    /**
     * Membuat constructor untuk ButirPenilaian
     * @param nilai nilai butir
     * @param terlambat indikator keterlambatan pengumpulan
     */
    public ButirPenilaian(double nilai, boolean terlambat) {
        this.nilai = nilai;
        this.terlambat = terlambat;
    }

    /**
     * Mengembalikan butir penilaian yang sudah disesuaikan dengan keterlambatan
     * (jika terlambat).
     * @return nilai akhir.
     */
    public double getNilai() {
        if (this.nilai >= 0) {
            if (this.terlambat) {
                return this.nilai - (this.nilai * PENALTI_KETERLAMBATAN)/100;
            } else {
                return this.nilai;
            }
        } else {
            return 0.00;
        }
    }

    @Override
    public String toString() {
        if (this.terlambat) {
            return String.format("%.2f (T)", this.getNilai());
        } else {
            return String.format("%.2f", this.getNilai());
        }
    }
}