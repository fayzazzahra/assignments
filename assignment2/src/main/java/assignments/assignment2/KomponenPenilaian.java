package assignments.assignment2;

/**
* KomponenPenilaian terdiri atas beberapa ​ButirPenilaian.
* ​KomponenPenilaian memiliki ​nama sebagai label komponen dan ​
* bobot yang menandakan persentase kontribusi komponen terhadap nilai akhir.
* @author  Fayza Azzahra Robby
* @version 1.0
* @since 2020-03-12
*/
public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    /**
     * Membuat constructor untuk KomponenPenilaian
     * @param nama nama komponen penilaian
     * @param banyakButirPenilaian banyaknya butir penilaian
     * @param bobot bobot tiap butir penilaian.
     */
    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        this.nama = nama;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        this.bobot = bobot;
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    /**
     * Masukkan butir ke butirPenilaian pada index ke-idx.
     * @return void
     */
    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        butirPenilaian[idx] = butir;
    }

    /**
     * Mendapatkan nama komponenPenilaian.
     * @return nama KomponenPenilaian.
     */
    public String getNama() {
        return this.nama;
    }

    /**
     * Mengembalikan nilai rerata butir berdasarkan yang sudah di-input.
     * @return rata-rata butir penilaian 
     */
    public double getRerata() {
        double temp = 0.0;
        int pembagi = 0;
        for (int i = 0; i < butirPenilaian.length; i++) {
            if (butirPenilaian[i] != null) {
            temp += butirPenilaian[i].getNilai();
            pembagi++;
            }
        }
        if (pembagi == 0) {
            return 0.00;
        } else {
            return temp/pembagi;
        }
    }

    /**
     * Mengembalikan nilai rerata butir yang sudah dikali dengan bobot komponen.
     * @return kontribusi nilai akhir.
     */
    public double getNilai() {
        return (this.getRerata() * this.bobot)/100;
    }

    /**
     * Mengembalikan detail KomponenPenilaian
     * @return butir penilaian tiap komponen, rata-rata butir, kontribusi komponen terhadap nilai akhir.
     */
    public String getDetail() {
        String mainDetail = "~~~ " + this.nama + " (" + this.bobot + "%)" + " ~~~\n";
        String kontribusiNilai = "\nKontribusi Nilai Akhir: " + String.format("%.2f", getNilai()) + "\n";
        String rerata = "Rerata: " + String.format("%.2f", this.getRerata());
        if (butirPenilaian.length == 1) {
            if (butirPenilaian[0] != null) {
                mainDetail += this.nama + ": " + String.format("%s\n", butirPenilaian[0]);
                mainDetail += String.format("Kontribusi Nilai Akhir: %.2f\n", getNilai());   
                return mainDetail;
            } else {
                mainDetail += this.nama + ": 0.00\n";
                mainDetail += String.format("Kontribusi Nilai Akhir: %.2f\n", getNilai());
                return mainDetail; 
            }
        } else {
            if (butirPenilaian!=null) {
                String detailButir = "";
                for (int j = 0; j < butirPenilaian.length; j++) {
                    if (butirPenilaian[j] != null){
                        detailButir += this.nama + " " + (j+1) + ": " + butirPenilaian[j] + "\n";
                    }
                }
            return mainDetail + detailButir + rerata + kontribusiNilai;
            } else {
                return mainDetail;
            }
        }
    }

    /**
     * Override toString method untuk print rerata komponen penilaian.
     * @return rerata komponen penilaian.
     */
    @Override
    public String toString() {
        return String.format("Rerata %s: %.2f", this.nama, this.getRerata());
    }

}

