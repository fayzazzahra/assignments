package assignments.assignment2;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

/**
* AsistenDosen diidentifikasi dengan kode asdos dan namanya.
* @author  Fayza Azzahra Robby
* @version 1.0
* @since 2020-03-12
*/
public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    /**
     * Membuat constructor untuk AsistenDosen.
     * @param kode kode asisten dosen.
     * @param nama nama asisten dosen.
     */    
    public AsistenDosen(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    /**
     * Mengembalikan kode asisten dosen yang dicari.
     * @return kode AsistenDosen.
     */
    public String getKode() {
        return this.kode;
    }

    /**
     * Menambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan urutan.
     */
    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
    }

    /**
     * Mengembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
     * @return mahasiswa dengan npm yang dicari.
     */
    public Mahasiswa getMahasiswa(String npm) {
        for (Mahasiswa siswa : this.mahasiswa) {
            if (siswa.getNpm().equals(npm)) {
                return siswa;
            }
        }
        return null;
    }

    /**
     * Merekap semua mahasiswa yang menjadi tanggung jawabnya.
     * @return rekapan mahasiswa.
     */
    public String rekap() {
        String y = "";
        for (Mahasiswa mhs : this.mahasiswa) {
            y += mhs.toString() + "\n";
            y += mhs.rekap() + "\n";
        }
        return y;
    }

    /**
     * Override toString method untuk print data AsistenDosen.
     * @return kode dan nama asisten dosen.
     */
    public String toString() {
        return this.kode + " - " + this.nama;
    }
}
