package assignments.assignment3;

public abstract class Benda extends Carrier{
  
    protected int persentaseMenular = 0;

    /**
     * Membuat constructor untuk class Benda, dengan mengakses constructor superclassnya (Carrier)
     * @param nama nama benda
     */
    public Benda(String name){
        super(name, "Benda");
    }

    /**
     * Menambah persentase suatu benda jika berinteraksi dengan objek manusia positif
     */
    public abstract void tambahPersentase();

    /**
     * Mengembalikan nilai dari atribut persentase menular
     * @return persentase menular suatu benda
     */
    public int getPersentaseMenular(){
        return persentaseMenular;
    }
    
    /**
     * Mengupdate nilai persentase menular suatu benda setelah berinteraksi
     * @param persentase menular suatu benda yang akan di-set
     */
    public void setPersentaseMenular(int persentase) {
        persentaseMenular = persentase;
    }
    
}