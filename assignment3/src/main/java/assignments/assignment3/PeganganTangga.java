package assignments.assignment3;

public class PeganganTangga extends Benda{
      
    /**
     * Membuat constructor untuk class PeganganTangga, dengan mengakses constructor superclassnya (Benda)
     * @param nama nama pegangan tangga
     */
    public PeganganTangga(String name){
        super(name);
    }
    
    /**
     * {@inheritDoc}
     * Menambah 20 persen
     */
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 20);
        if (persentaseMenular >= 100) {
            ubahStatus("Positif");
        }
    }

    /**
     * Mengembalikan kategori dari class PeganganTangga
     * @return Pegangan Tangga
     */
    public String getKategori() {
        return "Pegangan Tangga";
    }
    
    /**
     * Mencetak kategori dan nama objek
     * @return PEGANGAN TANGGA + nama objek
     */
    public String toString() {
        return "PEGANGAN TANGGA " + getNama();
    }
}