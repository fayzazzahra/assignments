package assignments.assignment3;

import java.io.*;

public class InputOutput{
  	
    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile; 
    private World world;
    

    /**
     * Constructor untuk class InputOutput
     * @param inputType tipe input yang diinginkan, text atau terminal
	 * @param inputFile
     * @param outputType
     * @param outputFile
     */
    public InputOutput(String inputType, String inputFile, String outputType, String outputFile){
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        setBufferedReader(inputType);
        setPrintWriter(outputType);
        
    }

    /**
     * Membuat BufferedReader bergantung inputType (I/O text atau input terminal) 
     * @param inputType tipe input yang diinginkan, text atau terminal
     */
    public void setBufferedReader(String inputType){
        try {
            if (inputType.equalsIgnoreCase("text")) {
                br = new BufferedReader(new FileReader(this.inputFile));
            } else if (inputType.equalsIgnoreCase("terminal")) {
                br = new BufferedReader(new InputStreamReader(System.in));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Membuat PrintWriter bergantung inputType (I/O text atau output terminal) 
     * @param outputType tipe output yang diinginkan, text atau terminal
     */
    public void setPrintWriter(String outputType){
        try {
            if (outputType.equalsIgnoreCase("text")) {
                pw = new PrintWriter(this.outputFile);
            } else if (outputType.equalsIgnoreCase("terminal")) {
                pw = new PrintWriter(new OutputStreamWriter(System.out));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Program utama untuk InputOutput
     */
    public void run() throws IOException {
        world = new World();
        try {
            String input = br.readLine();
            while (!input.equalsIgnoreCase("exit")) {
                String[] perkata = input.split(" ");
                if (perkata[0].equalsIgnoreCase("add")) {
                    world.createObject(perkata[1], perkata[2]);
                } else if (perkata[0].equalsIgnoreCase("interaksi")) {
                    queryInteraksi(perkata[1], perkata[2]);
                } else if (perkata[0].equalsIgnoreCase("positifkan")) {
                    queryPositifkan(perkata[1]);
                } else if (perkata[0].equalsIgnoreCase("sembuhkan")) {
                    querySembuhkan(perkata[1], perkata[2]);
                } else if (perkata[0].equalsIgnoreCase("bersihkan")) {
                    queryBersihkan(perkata[1], perkata[2]);
                } else if (perkata[0].equalsIgnoreCase("rantai")) {
                    pw.println(queryRantai(perkata[1]));
                } else if (perkata[0].equalsIgnoreCase("total_kasus_dari_objek")) {
                    pw.println(queryTotalKasus(perkata[1]));
                } else if (perkata[0].equalsIgnoreCase("aktif_kasus_dari_objek")) {
                    pw.println(queryAktifKasus(perkata[1]));
                } else if (perkata[0].equalsIgnoreCase("total_sembuh_manusia")) {
                    pw.println(queryTotalSembuh());
                } else if (perkata[0].equalsIgnoreCase("total_sembuh_petugas_medis")) {
                    pw.println(querySembuhPM(perkata[1]));
                } else if (perkata[0].equalsIgnoreCase("total_bersih_cleaning_service")) {
                    pw.println(queryBersihCS(perkata[1]));
                }
                input = br.readLine();
                
            } 
        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();
        pw.close();
    }
    
    /**
     * Menginteraksikan dua objek dengan memanggil method interaksi pada class Carrier
     * @param objek1 yang akan di get tipe carriernya melalui class World
     * @param objek2 yang akan di get tipe carriernya melalui class World
     */
    private void queryInteraksi(String o1, String o2) {
        Carrier x = world.getCarrier(o1);
        Carrier y = world.getCarrier(o2);
        if (world.getListCarrier().contains(x) && world.getListCarrier().contains(y)) {
            if (x.getStatusCovid().equals("Positif")) {
                x.interaksi(y);
            } else if (y.getStatusCovid().equals("Positif")) {
                y.interaksi(x);
            }
        }
    }

    /**
     * Mengubah status suatu objek menjadi positif untuk pertama kali dalam suatu wilayah
     * @param objek yang akan dipositifkan
     */
    private void queryPositifkan(String objek) {
        Carrier x = world.getCarrier(objek);
        if (world.getListCarrier().contains(x) && x.getTipe().equals("Manusia")) {
            x.ubahStatus("Positif");
        }
    }

    /**
     * Memanggil method obati pada class PetugasMedis, untuk menyembuhkan objek
     * @param objek1 (petugas medis) yang akan menyembuhkan objek2
     * @param objek2 yang akan disembuhkan oleh petugas medis
     */
    private void querySembuhkan(String o1, String o2) {
        Carrier x = world.getCarrier(o1);
        Carrier y = world.getCarrier(o2);
        if (world.getListCarrier().contains(x) && world.getListCarrier().contains(y)) {
            if (x.getKategori().equals("Petugas Medis") && y.getStatusCovid().equals("Positif")) {
                PetugasMedis pMedis = (PetugasMedis) x;
                Manusia m = (Manusia) y;
                pMedis.obati(m);
            }
        }
    }

    /**
     * Memanggil method bersihkan pada class CleaningService, untuk membersihkan objek
     * @param objek1 (cleaning service) yang akan membersihkan objek2
     * @param objek2 yang akan dibersihkan oleh cleaning service
     */
    private void queryBersihkan(String o1, String o2) {
        Carrier x = world.getCarrier(o1);
        Carrier y = world.getCarrier(o2);
        if (world.getListCarrier().contains(x) && world.getListCarrier().contains(y)) {
            if (x.getKategori().equals("Cleaning Service")) {
                CleaningService cService = (CleaningService) x;
                cService.bersihkan((Benda) y);
            }
        } 
    }

    /**
     * Mencetak rantai penularan yang menyebabkan obj1 tertular covid
     * @param objek1
     */
    private String queryRantai(String obj1) {
        Carrier c = world.getCarrier(obj1);
        String x;
        try {
            x = "Rantai penyebaran " + c.toString() + ": ";
            for (Carrier car : c.getRantaiPenular()) {
                x += car.toString() + " -> ";
            }
            x += c.toString();
        } catch (BelumTertularException e) {
            x = e.getMessage();
        }
        return x;
    }

    /**
     * Mencetak total jumlah kasus yang disebabkan oleh o1 (positif maupun sudah sembuh)
     * @param objek1
     */
    private String queryTotalKasus(String o1) {
        Carrier c = world.getCarrier(o1);
        String x = "";
        if (world.getListCarrier().contains(c)) {
            x += c.toString() + " telah menyebarkan virus COVID ke " + c.getTotalKasusDisebabkan() + " objek";
        }
        return x;
    }

    /**
     * Mencetak jumlah kasus disebabkan oleh o1 yang masih positif (belum sembuh)
     * @param objek1
     */
    private String queryAktifKasus(String o1) {
        Carrier c = world.getCarrier(o1);
        String x = "";
        if (world.getListCarrier().contains(c)) {
            x += c.toString() + " telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak " + c.getAktifKasusDisebabkan() + " objek";
        }
        return x;
    }
    
    /**
     * Mencetak total kasus manusia yang sembuh
     */
    private String queryTotalSembuh() {
        String x = "";
        x = "Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: " + Manusia.getJumlahSembuh() + " kasus";
        return x;
    }

    /**
     * Mencetak total jumlah kasus yang disembuhkan oleh petugas medis o1
     * @param objek1
     */
    private String querySembuhPM(String o1) {
        Carrier c = world.getCarrier(o1);
        String x = "";
        //if (world.getListCarrier().contains(c) && c.getKategori().equals("Petugas Medis")) {
        if (world.getListCarrier().contains(c) && c.getKategori().equals("Petugas Medis")) {
            PetugasMedis pMedis = (PetugasMedis) c;
            x = c.toString() + " menyembuhkan " + pMedis.getJumlahDisembuhkan() + " manusia";
        }
        return x;
    }

    /**
     * Mencetak total jumlah kasus yang dibersihkan oleh cleaning service o1
     * @param objek1
     */
    private String queryBersihCS(String o1) {
        Carrier c = world.getCarrier(o1);
        String x = "";
        if (world.getListCarrier().contains(c) && c.getKategori().equals("Cleaning Service")) {
        CleaningService cService = (CleaningService) c;
            x += c.toString() + " membersihkan " + cService.getJumlahDibersihkan() + " benda";
        }
        return x;
    }

}