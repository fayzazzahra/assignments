package assignments.assignment3;

public class Negatif implements Status{
  
    /**
     * Mengembalikan status covid
     * @return Negatif
     */
    public String getStatus() {
        return "Negatif";
    }

    /**
     * objek penular yang berstatus negatif melakukan interaksi dengan object tertular
     * @param penular 
     * @param tertular
     */
    public void tularkan(Carrier penular, Carrier tertular) {
        try {
            if (penular.getTipe().equals("Manusia") && tertular.getTipe().equals("Benda")) {
                Benda bnd = (Benda) tertular;
                if (bnd.getPersentaseMenular() >= 100) {
                    tertular.ubahStatus("Positif");
                }
                tertular.setRantaiPenular(penular.getRantaiPenular());
                tertular.getRantaiPenular().add(penular);

            } else if (penular.getTipe().equals("Benda") && tertular.getTipe().equals("Manusia")) {
                Benda bnd =  (Benda) penular;
                bnd.tambahPersentase();
                if (bnd.getPersentaseMenular() >= 100) {
                    bnd.ubahStatus("Positif");
                    tertular.setRantaiPenular(penular.getRantaiPenular());
                    tertular.getRantaiPenular().add(penular);
                }

            } else if (penular.getTipe().equals("Benda") && tertular.getTipe().equals("Benda")) {
                return;
            }
        } catch (BelumTertularException e) {}
    }
}

