package assignments.assignment3;

public class Jurnalis extends Manusia{
      
    /**
     * Membuat constructor untuk class Jurnalis, dengan mengakses constructor superclassnya (Manusia)
     * @param nama nama jurnalis
     */
    public Jurnalis(String name){
        super(name);
    }

    /**
     * Mengembalikan kategori dari class Jurnalis
     * @return Jurnalis
     */
    public String getKategori() {
        return "Jurnalis";
    }

    /**
     * Mencetak kategori dan nama objek
     * @return JURNALIS + nama objek
     */
    @Override
    public String toString() {
        return "JURNALIS " + getNama();
    }
    
}