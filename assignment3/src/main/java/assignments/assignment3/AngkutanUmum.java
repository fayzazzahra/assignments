package assignments.assignment3;

public class AngkutanUmum extends Benda {
      
    /**
     * Membuat constructor untuk class AngkutanUmum, dengan mengakses constructor superclassnya (Benda)
     * @param nama nama angkutan umum
     */
    public AngkutanUmum(String name){
        super(name);
    }

    /**
     * {@inheritDoc}
     * Menambah 35 persen
     */
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 35);
        if (persentaseMenular >= 100) {
            ubahStatus("Positif");
        }
    }

    /**
     * Mengembalikan kategori dari class AngkutanUmum
     * @return Angkutan Umum
     */
    public String getKategori() {
        return "Angkutan Umum";
    }

    /**
     * Mencetak kategori dan nama objek
     * @return ANGKUTAN UMUM + nama objek
     */
    @Override
    public String toString() {
        return "ANGKUTAN UMUM " + getNama();
    }
}