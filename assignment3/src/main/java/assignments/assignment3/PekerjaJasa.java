package assignments.assignment3;

public class PekerjaJasa extends Manusia{
      
    /**
     * Membuat constructor untuk class PekerjaJasa, dengan mengakses constructor superclassnya (Manusia)
     * @param nama nama pekerja jasa
     */
    public PekerjaJasa(String nama){
        super(nama);
    }

    /**
     * Mengembalikan kategori dari class PekerjaJasa
     * @return Pekerja Jasa
     */
    public String getKategori() {
        return "Pekerja Jasa";
    }
      
    /**
     * Mencetak kategori dan nama objek
     * @return PEKERJA JASA + nama objek
     */
    @Override
    public String toString() {
        return "PEKERJA JASA " + getNama();
    }
}