package assignments.assignment3;

public abstract class Manusia extends Carrier {

    private static int jumlahSembuh = 0;
    
    /**
     * Membuat constructor untuk class Manusia, dengan mengakses constructor superclassnya (Carrier)
     * @param nama nama manusia
     */
    public Manusia(String nama){
        super(nama, "Manusia");
    }
    
    /**
     * Menambahkan nilai pada atribut jumlahSembuh.
     */
    public void tambahSembuh(){
        jumlahSembuh++;
    }

    /**
     * Mengembalikan nilai dari atribut jumlahSembuh
     * @return total jumlah pasien covid yang sudah sembuh
     */
    public static int getJumlahSembuh() {
        return jumlahSembuh;
    }
    
}