package assignments.assignment3;

public class TombolLift extends Benda{      

    /**
     * Membuat constructor untuk class TombolLift, dengan mengakses constructor superclassnya (Benda)
     * @param nama nama Tombol Lift
     */
    public TombolLift(String name){
        super(name);
    }

    /**
     * {@inheritDoc}
     * Menambah 20 persen
     */
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 20);
        if (persentaseMenular >= 100) {
            ubahStatus("Positif");
        }
    }

    /**
     * Mengembalikan kategori dari class TombolLift
     * @return TombolLift
     */
    public String getKategori() {
        return "Tombol Lift";
    }

    /**
     * Mencetak kategori dan nama objek
     * @return TOMBOL LIFT + nama objek
     */
    public String toString() {
        return "TOMBOL LIFT " + getNama();
    }

}