package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World {

    public List<Carrier> listCarrier;

    /**
     * Membuat constructor untuk class World
     */
    public World() {
        listCarrier = new ArrayList<Carrier>();
    }

    /**
     * Membuat objek sesuai dengan parameter yang diberikan
     * @param tipe objek yang ingin dibuat
     * @param nama objek yang ingin dibuat
     */
    public Carrier createObject(String tipe, String nama) {
        if (tipe.equalsIgnoreCase("petugas_medis")) {
            PetugasMedis pMedis = new PetugasMedis(nama);
            listCarrier.add(pMedis);
            return pMedis;
        } else if (tipe.equalsIgnoreCase("pekerja_jasa")) {
            PekerjaJasa pJasa = new PekerjaJasa(nama);
            listCarrier.add(pJasa);
            return pJasa;
        } else if (tipe.equalsIgnoreCase("jurnalis")) {
            Jurnalis jurnalis = new Jurnalis(nama);
            listCarrier.add(jurnalis);
            return jurnalis;
        } else if (tipe.equalsIgnoreCase("ojol")) {
            Ojol ojol = new Ojol(nama);
            listCarrier.add(ojol);
            return ojol;
        } else if (tipe.equalsIgnoreCase("cleaning_service")) {
            CleaningService cService = new CleaningService(nama);
            listCarrier.add(cService);
            return cService;
        } else if (tipe.equalsIgnoreCase("pegangan_tangga")) {
            PeganganTangga pTangga = new PeganganTangga(nama);
            listCarrier.add(pTangga);
            return pTangga;
        } else if (tipe.equalsIgnoreCase("pintu")) {
            Pintu pintu = new Pintu(nama);
            listCarrier.add(pintu);
            return pintu;
        } else if (tipe.equalsIgnoreCase("tombol_lift")) {
            TombolLift tLift = new TombolLift(nama);
            listCarrier.add(tLift);
            return tLift;
        } else if (tipe.equalsIgnoreCase("angkutan_umum")) {
            AngkutanUmum aUmum = new AngkutanUmum(nama);
            listCarrier.add(aUmum);
            return aUmum;
        } else {
            return null;
        }
    }
            
    /**
     * Mengambil carrier sesuai nama yang diberikan
     * @param nama yang diberikan
     */
    public Carrier getCarrier(String nama){
        for (Carrier carrier : listCarrier) {
            if (nama.equals(carrier.getNama())) {
                return carrier;
            }
        }
        return null;
    }

    /**
     * Mengembalikan List bertipe Carrier berisi carrier yang terlah dibuat.
     * @return listcarrier
     */
    public List<Carrier> getListCarrier() {
        return listCarrier;
    }

}