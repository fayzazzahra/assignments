package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class PetugasMedis extends Manusia {
  		
    private int jumlahDisembuhkan;

    /**
     * Membuat constructor untuk class PetugasMedis, dengan mengakses constructor superclassnya (Manusia)
     * @param nama nama petugas medis
     */
    public PetugasMedis(String nama) {
        super(nama);
    }

    /**
     * objek PetugasMedis menyembuhkan manusia
     * @param manusia yang akan disembuhkan oleh petugas medis
     */
    public void obati(Manusia manusia) {
        if (manusia.getStatusCovid().equals("Positif")) { 
            try {
                List<Carrier> lst = new ArrayList<Carrier>();
                for (Carrier c : manusia.getRantaiPenular()) {
                    if (!lst.contains(c)) {
                        lst.add(c);
                        c.setAktifKasusDisebabkan(c.getAktifKasusDisebabkan() - 1);
                    }
                }
            } catch (BelumTertularException e) {}     
            manusia.ubahStatus("Negatif");
        }
        jumlahDisembuhkan++;
        manusia.kosongkanRantaiPenular();
        manusia.tambahSembuh();
    }


    /**
     * Mengembalikan jumlah pasien yang disembuhkan oleh petugas medis
     * @return jumlahDisembuhkan
     */
    public int getJumlahDisembuhkan() {
        return jumlahDisembuhkan;
    }

    /**
     * Mengembalikan kategori dari class PetugasMedis
     * @return Petugas Medis
     */
    public String getKategori() {
        return "Petugas Medis";
    }
    
    /**
     * Mencetak kategori dan nama objek
     * @return PETUGAS MEDIS + nama objek
     */
    @Override
    public String toString() {
        return "PETUGAS MEDIS " + getNama();
    }

}