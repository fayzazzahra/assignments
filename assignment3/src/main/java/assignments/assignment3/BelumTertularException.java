package assignments.assignment3;

public class BelumTertularException extends Exception	{

	private String message;

	public BelumTertularException(String nama) {
		this.message = "assignments.assignment3.BelumTertularException: " + nama + " berstatus negatif";

	}

	public String getMessage() {
		return this.message;
	}
}