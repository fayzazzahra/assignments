package assignments.assignment3;

public class Ojol extends Manusia {
      
    /**
     * Membuat constructor untuk class Ojol, dengan mengakses constructor superclassnya (Manusia)
     * @param nama nama ojol
     */
    public Ojol(String name){
        super(name);
    }

    /**
     * Mengembalikan kategori dari class Ojol
     * @return Ojol
     */
    public String getKategori() {
        return "Ojol";
    }

    /**
     * Mencetak kategori dan nama objek
     * @return OJOL + nama objek
     */
    @Override
    public String toString() {
        return "OJOL " + getNama();
    }
    
}