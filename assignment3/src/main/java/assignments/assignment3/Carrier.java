package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier {

    private String nama;
    private String tipe;
    private Status statusCovid = new Negatif();
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    /**
     * Membuat constructor untuk class Carrier
     * @param nama nama carrier
	 * @param tipe tipe carrier (manusia atau benda)
     */
    public Carrier(String nama, String tipe){
        this.nama = nama;
        this.tipe = tipe;
        this.totalKasusDisebabkan = 0;
        this.aktifKasusDisebabkan = 0;
        this.rantaiPenular = new ArrayList<Carrier>();
    }

    /**
     * Mengembalikan nama yang dimiliki oleh carrier.
     * @return nama dari carrier.
     */
    public String getNama(){
        return this.nama;
    }

    /**
     * Mengembalikan tipe yang dimiliki oleh carrier.
     * @return tipe carrier.
     */
    public String getTipe(){
        return this.tipe;
    }

    /**
     * Mengembalikan status covid yang dimiliki oleh carrier.
     * @return status covid carrier.
     */
    public String getStatusCovid(){
        return statusCovid.getStatus();
    }

    /**
     * Mengembalikan aktif kasus disebabkan oleh carrier
     * @return aktif kasus disebabkan
     */
    public int getAktifKasusDisebabkan(){
        return this.aktifKasusDisebabkan;
    }

    /**
     * Mengupdate nilai atribut aktifkasusdisebabkan
     * @param aktifkasus yang telah diupdate di class Positif
     */
    public void setAktifKasusDisebabkan(int aktifkasus) {
        this.aktifKasusDisebabkan = aktifkasus;
    }

    /**
     * Mengembalikan total kasus yang disebabkan oleh carrier
     * @return total kasus disebabkan
     */
    public int getTotalKasusDisebabkan(){
        return this.totalKasusDisebabkan;
    }

    /**
     * Mengupdate nilai atribut totalkasusdisebabkan
     * @param totalkasus yang telah diupdate di class Positif
     */
    public void setTotalKasusDisebabkan(int totalkasus) {
        this.totalKasusDisebabkan = totalkasus;
    }

    /**
     * Mengembalikan List bertipe Carrier yang berisi rantai penular.
     * @return list rantai penular.
     */
    public List<Carrier> getRantaiPenular() throws BelumTertularException {
        if (getStatusCovid().equalsIgnoreCase("Negatif")) {
            throw new BelumTertularException(this.toString());
        }
        return this.rantaiPenular;
    }

    /**
     * Mengupdate nilai atribut rantaipenular
     * @param lst list yang akan diset jadi rantaipenular yang baru
     */
    public void setRantaiPenular(List<Carrier> lst) {
        for (Carrier carrier : lst) {
            this.rantaiPenular.add(carrier);
        }
    }

    /**
     * Mengosongkan rantaipenular ketika objek manusia disembuhkan
     */
    public void kosongkanRantaiPenular() {
        this.rantaiPenular = new ArrayList<Carrier>();
    }

    /**
     * Mengubah status covid carrier dari positif ke negatif dan sebaliknya.
     * @param status terkini carrier yang akan diubah.
     */
    public void ubahStatus(String status){
        if (status.equals("Positif")) {
            this.statusCovid = new Positif();
        } else {
            this.statusCovid = new Negatif();
        }
    }

    /**
     * Objek berinteraksi dengan objek lain
     * @param Carrier lain yang berinterkasi dengannya
     */
    public void interaksi(Carrier lain) {
        statusCovid.tularkan(this, lain);
    }
    
    /**
     * Mengembalikan kategori dari subclass manusia dan benda
     */
    public abstract String getKategori();

    public abstract String toString();

}