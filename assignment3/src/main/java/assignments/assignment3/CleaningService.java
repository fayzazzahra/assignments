package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;


public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    /**
     * Membuat constructor untuk class CleaningService, dengan mengakses constructor superclassnya (Manusia)
     * @param nama nama cleaning service
     */
    public CleaningService(String nama){
        super(nama);
    }

    /**
     * objek CleaningService membersihkan benda
     * @param benda yang akan dibersihkan oleh cleaning service
     */
    public void bersihkan(Benda benda) {
        jumlahDibersihkan++;
        if (benda.getStatusCovid().equals("Positif")) {
            try {
                List<Carrier> lst = new ArrayList<Carrier>();
                for (Carrier carrier : benda.getRantaiPenular()) {
                    if(!lst.contains(carrier)) {
                        carrier.setAktifKasusDisebabkan(carrier.getAktifKasusDisebabkan() - 1);
                        lst.add(carrier);
                    }
                }
            } catch (BelumTertularException e) {
            }
            benda.setPersentaseMenular(0);
            benda.ubahStatus("Negatif");
        } else if (benda.getStatusCovid().equals("Negatif")) {
            benda.setPersentaseMenular(0);
        }
    }

    /**
     * Mengembalikan jumlah benda yang dibersihkan oleh cleaning service
     * @return jumlahDibersihkan
     */
    public int getJumlahDibersihkan() {
        return jumlahDibersihkan;
    }

    /**
     * Mengembalikan kategori sesuai pekerjaan
     * @return Cleaning Service
     */
    public String getKategori() {
        return "Cleaning Service";
    }

    /**
     * Mencetak kategori dan nama objek
     * @return CLEANING SERVICE + nama objek
     */
    @Override
    public String toString() {
        return "CLEANING SERVICE " + getNama();
    }

}