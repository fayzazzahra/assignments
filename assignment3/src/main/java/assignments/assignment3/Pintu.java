package assignments.assignment3;

public class Pintu extends Benda{
    
    /**
     * Membuat constructor untuk class Pintu, dengan mengakses constructor superclassnya (Benda)
     * @param nama nama pintu
     */
    public Pintu(String name){
        super(name);
    }

    /**
     * {@inheritDoc}
     * Menambah 30 persen
     */
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 30);
        if (persentaseMenular >= 100) {
            ubahStatus("Positif");
        } 
    }

    /**
     * Mengembalikan kategori dari class Pintu
     * @return Pintu
     */
    public String getKategori() {
        return "Pintu";
    }

    /**
     * Mencetak kategori dan nama objek
     * @return PINTU + nama objek
     */
    public String toString() {
        return "PINTU " + getNama();
    }
}