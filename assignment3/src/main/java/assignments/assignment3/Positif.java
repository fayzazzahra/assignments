package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class Positif implements Status {
  
    /**
     * Mengembalikan status covid
     * @return Positif
     */
    public String getStatus(){
        return "Positif";
    }

    /**
     * objek penular yang berstatus positif melakukan interaksi dengan object tertular
     * @param penular 
     * @param tertular
     */
    public void tularkan(Carrier penular, Carrier tertular){
        if (penular.getTipe().equals("Manusia") && tertular.getTipe().equals("Manusia")) {
            tertular.ubahStatus("Positif");
            addRantaiAktifTotal(penular, tertular);

        } else if (penular.getTipe().equals("Manusia") && tertular.getTipe().equals("Benda")) {
            Benda bnd = (Benda) tertular;
            bnd.tambahPersentase();
            if (bnd.getPersentaseMenular() >= 100) {
                addRantaiAktifTotal(penular, tertular);
                tertular.ubahStatus("Positif");
            }

        } else if (penular.getTipe().equals("Benda") && tertular.getTipe().equals("Manusia")) {
            Benda bnd =  (Benda) penular;
            if (bnd.getPersentaseMenular() >= 100) {
                tertular.ubahStatus("Positif");
                addRantaiAktifTotal(penular, tertular);
            }
            
        } else if (penular.getTipe().equals("Benda") && tertular.getTipe().equals("Benda")) {
            return;
        }
    }

    /**
     * Menge-set rantai penularan dan atribut aktifkasusdisebabkan dan totalkasusdisebabkan
     * @param penular 
     * @param tertular
     */
    private void addRantaiAktifTotal(Carrier penular, Carrier tertular) {
        try {
            if (penular.getRantaiPenular().isEmpty()) {
                List<Carrier> temp = new ArrayList<Carrier>();
                temp.add(penular);
                tertular.setRantaiPenular(temp);
            } else {
                List<Carrier> temp = new ArrayList<Carrier>();
                for (Carrier c : penular.getRantaiPenular()) {
                    temp.add(c);
                }
                temp.add(penular);
                tertular.setRantaiPenular(temp);
            }
            
            List<Carrier> lst = new ArrayList<Carrier>();
            for (Carrier c : tertular.getRantaiPenular()) {
                if (!lst.contains(c)) {
                    c.setTotalKasusDisebabkan(c.getTotalKasusDisebabkan() + 1);
                    c.setAktifKasusDisebabkan(c.getAktifKasusDisebabkan() + 1);
                    lst.add(c);
                }
            }
        } catch (BelumTertularException e) {}
        
    }
    
}
            
    
